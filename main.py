import threading
from numpy import random
from time import *
import time


class ThreadClass:
    def __init__(self, thread, pt):
        # initiate default value
        self.thread = thread
        self.pt = pt


def process(delayTime):
    print(delayTime)
    start = time.time()
    print('process time is', delayTime)
    sleep(delayTime)
    end = time.time()
    print('time elapsed', end - start)
    print('thread done')


def main():
    global processTime
    numOfThreads = 3
    roundRabinTimeSlice = 2
    # creating thread list
    thread_list = []
    for i in range(numOfThreads):
        processTime = random.randint(10)
        # creating thread
        thread = threading.Thread(target=process, args=(processTime,))
        # creating thread obj
        threadObj = ThreadClass(thread, processTime)
        thread_list.append(threadObj)
        # First Come (uncomment these)
    #     # thread.start()
    #     # thread.join()
    #
    # # ordering the list based on SJF
    # # sorting base on process time
    # orderList = sorted(thread_list, key=lambda thread: thread.pt)
    # for i in range(numOfThreads):
    #     orderList[i].thread.start()
    #     # waiting for the thread to finish
    #     orderList[i].thread.join()

    #     Round Rabin
    for i in range(numOfThreads):
        print(thread_list[i].pt)
        print('line')
        if thread_list[i].pt - roundRabinTimeSlice > 0:
            thread_list[i].pt = thread_list[i].pt - roundRabinTimeSlice
            thread_list[i].thread.start()
        print(thread_list[i].pt)


if __name__ == '__main__':
    main()
